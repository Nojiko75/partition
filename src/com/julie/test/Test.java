/**
 * 
 */
package com.julie.test;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import com.julie.lib.Tools;

import exception.LengthException;
import exception.NullListException;
import junit.framework.TestCase;

/**
 * @author julie
 *
 */
public class Test extends TestCase {
	
	private int[] list = {1,2,3,4,5};
	
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	public void testPartition_2() throws Exception {
		String expected = "[[1,2],[3,4],[5]]";
		int[][] partition = Tools.partition(list, 2);
		assertEquals(expected, Tools.toString(partition));
	}
	
	public void testPartition_3() throws Exception {
		String expected = "[[1,2,3],[4,5]]";
		int[][] partition = Tools.partition(list, 3);
		assertEquals(expected, Tools.toString(partition));
	}
	
	public void testPartition_1() throws Exception {
		String expected = "[[1],[2],[3],[4],[5]]";
		int[][] partition = Tools.partition(list, 1);
		assertEquals(expected, Tools.toString(partition));
	}
	
	public void testPartition_NullList() {
		try {
			Tools.partition(null, 1);
		} catch (Exception e) {
			exceptionRule.expect(NullListException.class);
		    exceptionRule.expectMessage("The list cannot be null");
		}
	}
	
	public void testPartition_LengthTooBig() {
		try {
			Tools.partition(list, 10);
		} catch (Exception e) {
			exceptionRule.expect(LengthException.class);
		    exceptionRule.expectMessage("The length must be less than the length of the list");
		}
	}
	
	public void testPartition_LengthTooSmall() {
		try {
			Tools.partition(list, -5);
		} catch (Exception e) {
			exceptionRule.expect(LengthException.class);
		    exceptionRule.expectMessage("The length must be greater than 0");
		}
	}
}
