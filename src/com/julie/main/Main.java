/**
 * 
 */
package com.julie.main;

import com.julie.lib.Tools;

import exception.LengthException;
import exception.NullListException;

/**
 * @author julie
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] list = {1,2,3,4,5,6,7,8,9,10,11};
		int[][] partition;
		try {
			partition = Tools.partition(list, 8);
			System.out.println(Tools.toString(partition));
		} catch (NullListException | LengthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
