/**
 * 
 */
package com.julie.lib;

import exception.LengthException;
import exception.NullListException;

/**
 * @author julie
 *
 */
public class Tools {

	/**
	 * 
	 * @param list the initial list to be divided into partitions
	 * @param length the length of partitions
	 * @return the partition
	 * @throws Exception
	 */
	public static int[][] partition(int[] list, int length) throws NullListException, LengthException {
		int[][] partition = null;
		if(list == null) {
			throw new NullListException("The list cannot be null");
		} else if(length < 1) {
			throw new LengthException("The length must be greater than 0");
		} else if(length > list.length) {
			throw new LengthException("The length must be less than the length of the list");
		} else {
			//the length of the list
			int listLength = list.length;
			
			//divide the list of length by the length parameter to get the length of partition
			Double dListLength = new Double(listLength);
			Double dLength = new Double(length);
			Double res = Math.ceil(dListLength / dLength);
			int partitionLength = res.intValue();
			partition = new int[partitionLength][length];
			
			//i: counter of the initial list
			//k: counter of the result partition
			for(int i=0, k=0; i<listLength; i+=length, k++) {
				int[] subList;
				if(listLength - i > length) {
					subList = new int[length];
				} else {
					subList = new int[listLength - i];
				}
				
				//j: counter of partitions
				for(int j=0; j<length; j++) {
					int index = i+j;
					
					if(index < listLength) {
						subList[j] = list[index]; 
					}
				}
				partition[k] = subList;
			}
		}
		
		return partition;
	}
	
	/**
	 * Display the partition
	 * 
	 * @param partition the partition to show
	 * @return
	 */
	public static String toString(int[][] partition) {
		String s = "[";
		for(int i=0; i<partition.length; i++) {
			s += "[";
			int[] sub = partition[i];
			for(int j=0; j<sub.length; j++) {
				s += partition[i][j];
				if(j < sub.length - 1) s += ","; 
			}
			s += "]";
			if(i < partition.length - 1) s += ",";
		}
		s += "]";

		return s;
	}
}
