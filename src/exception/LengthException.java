/**
 * 
 */
package exception;

/**
 * @author julie
 *
 */
public class LengthException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7256931826341250889L;

	/**
	 * 
	 */
	public LengthException() {
	}

	/**
	 * @param message
	 */
	public LengthException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public LengthException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public LengthException(String message, Throwable cause) {
		super(message, cause);
	}
}
