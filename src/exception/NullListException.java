/**
 * 
 */
package exception;

/**
 * @author julie
 *
 */
public class NullListException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3043581319777759917L;

	/**
	 * 
	 */
	public NullListException() {
	}

	/**
	 * @param message
	 */
	public NullListException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NullListException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NullListException(String message, Throwable cause) {
		super(message, cause);
	}
}
